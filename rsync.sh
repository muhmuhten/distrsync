#!/bin/sh
set -eu
from=rsync://ftp.halifax.rwth-aachen.de/freebsd
_rsync() {
	rsync --no-motd -af '. rsync.filter' --chown=0:0 "$@"
}
_die() {
	echo "$@" >&2
	exit 1
}

log=rsync.log.$$
case $0 in (*/*) cd "${0%/*}" ;; esac
_rsync -i "$from/releases/amd64/amd64/*" . | tee "$log"

empty=1
while read info path; do
	empty=
	case $info in ([!\>]*) continue ;; esac

	summed=
	while read file hash _; do
		[ "${path##*/}" = "$file" ] || continue
		sha256 -c "$hash" "$path"
		summed=1
	done < "${path%/*}/MANIFEST"

	[ "$summed" ] && continue
	[ "${path##*/}" = MANIFEST ] && continue
	_die "undeclared file: $path"
done < "$log"

[ "$empty" ] && rm "$log"

for dest; do
	echo "destination $dest"
	_rsync -i . "$dest"
done
